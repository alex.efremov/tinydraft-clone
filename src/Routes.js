import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import New from './containers/New';
import Edit from './containers/Edit';
import MainView from './containers/MainView';
import Show from './containers/Show';
import NotFound from './components/404';

// using withRouter on this component is a workaround to make react-router-dom work
// with react-redux's connect.
export default withRouter(() =>
  <Switch>
    <Route exact path='/' component={MainView} />
    <Route path='/new' component={New} />
    <Route path='/edit/:id' component={Edit} />
    <Route path='/:id' component={Show} />
    <Route component={NotFound} />
  </Switch>
);
