import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import './Header.css';
import Star from './Star';

export default withRouter(({ faved, onStarClick, onHeaderClick }) =>
  <div className="App-header">
    <h1><Link to='/' onClick={onHeaderClick}>tinydraft clone</Link></h1>
    <div className='Links'>
      <Link to='/new' className='underline'>
        New
      </Link>
      <a className='underline' onClick={onStarClick}>
        <Star faved={faved} />
      </a>
    </div>

  </div>
)
