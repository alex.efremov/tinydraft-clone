import React from 'react';
import { compose, withState, defaultProps, withHandlers } from 'recompose';
import { Link } from 'react-router-dom';

import Textarea from '../components/Textarea';
import './DraftInput.css';

const enhance = compose(
  defaultProps({
    value: ''
  }),
  withState('value', 'setValue', ({ value }) => value),
  withHandlers({
    onChange: ({ setValue }) => (e) => setValue(e.target.value),
    onSaveClick: ({ value, onSaveClick }) => () => {
        if (value) {
          onSaveClick(value);
        }
    }
  })
);

export default enhance(({ value, onChange, onSaveClick }) =>
<div className='Form'>
  <div className='Form-header'>
    <Link to='/'>
      <i className="fa fa-times" aria-hidden="true"></i>
    </Link>
    <div>
      <span className='Value-length'>{value.length}</span>
      <Link to='/' onClick={onSaveClick}>
        SAVE
      </Link>
    </div>
  </div>
  <div className='Form-textarea'>
    <Textarea
      autoFocus
      rows={2}
      onChange={onChange}
      value={value}
    />
  </div>
</div>
);
