import React from 'react';
import _ from 'lodash';
import { branch, renderComponent } from 'recompose';

import DraftPreview from './DraftPreview';
import NoDraftYet from './NoDraftYet';
import './DraftList.css'

const enhance = branch(
  ({ drafts }) => !Object.keys(drafts).length,
  renderComponent(NoDraftYet)
);

const DraftList = ({ drafts, onStartClick, onDeleteClick }) =>
  <div className='Draft-List'>
    {
      _.map(drafts, draft =>
        <DraftPreview
          key={draft.id}
          onStarClick={onStartClick}
          onDeleteClick={onDeleteClick}
          {...draft} />
      )
    }
  </div>

export default enhance(DraftList);
