import React from 'react';
import { Link } from 'react-router-dom';
import {
  compose,
  pure,
  withState,
  withHandlers,
  withProps } from 'recompose';
import Star from './Star';
import './DraftView.css';

const enhance = compose(
  pure,
  withState('faved', 'toggleStar', ({ faved }) => faved),
  withHandlers({
    onStarClick: ({ id, faved, onStarClick, toggleStar }) => e => {
      onStarClick(id, !faved);
      toggleStar(faved => !faved);
    },
    onDeleteClick: ({ id, onDeleteClick }) => e => onDeleteClick(id)
  }),
  withProps(({ faved }) => ({
    starClass: faved ? 'Star-faved' : ''
  }))
);

const DraftView = ({ id, value, faved, onStarClick, starClass, onDeleteClick }) =>
  <div className='Draft-View'>
    <Link to={`/${id}`} className='Draft-View-value-link'>
      <div className='Draft-View-value'>
        {value}
      </div>
    </Link>
    <div className='Draft-View-links'>
      <Link to={`/edit/${id}`} className='underline'>
        edit
      </Link>
      <a className='underline' onClick={onDeleteClick}>
        delete
      </a>
      <a
        href={`https://twitter.com/intent/tweet/?text=${value}`}
        className='underline'
        target="_blank"
        rel='noopener noreferrer'>
        tweet
      </a>
      <a className='underline' onClick={onStarClick} >
        <Star faved={faved} className={starClass} />
      </a>
    </div>
  </div>

export default enhance(DraftView);
