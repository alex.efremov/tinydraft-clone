import React from 'react';
import TransitionGroup from 'react-transition-group/TransitionGroup';

import Notification from './Notification';
import './index.css';


const NotificationStack = ({ notifications, enterTimeout, leaveTimeout, handleRemoveNotification, config }) =>
  <div className='notification-stack-wrapper'>
    <TransitionGroup component='div'>
      { notifications.map((n, i) =>
        <Notification
          id={n.key}
          clName='notification'
          onClick={handleRemoveNotification}
          title={n.title}
          message ={n.message}
          key={n.key}
          {...config} />
      )}
    </TransitionGroup>
  </div>;

export default NotificationStack;
