import React from 'react';
import PropTypes from 'prop-types';

import './Notification.css';

class Notification extends React.PureComponent {
  constructor() {
    super()
    this.state = {
      style: null
    }
    this.timeouts = [];
  }

  setTimeout = (cb, val) => this.timeouts.push(setTimeout(cb, val));

  onClick = () => this.props.onClick(this.props.id)

  componentWillEnter(callback) {
    this.setTimeout(() => this.props.onClick(this.props.id),this.props.autoLeaveTime);
    callback();
  }

  componentWillLeave(callback) {
    this.setState({
      style: {
        transition: `transform
        ${(this.props.leaveTime/1000)/2}s,
        height ${(this.props.leaveTime/1000)/2}s
        ${(this.props.leaveTime/1000)/2}s,
        margin-top ${(this.props.leaveTime/1000)/2}s
        ${(this.props.leaveTime/1000)/2}s`,
        transform: 'translateX(1000px)',
        height: '0',
        marginTop: '0px' //for smoother animation
      }
    })
    const to = setTimeout(() => callback(),this.props.leaveTime);
    this.timeouts.push(to);
  }

  componentDidMount() {
    this.setState({
      style: {
        transition: `transform ${this.props.enterTime/1000}s linear`,
        height: `${this.n.clientHeight}px`,
        transform: 'translateX(-1000px)'
      }
    })
  }

  componentWillUnmount() {
    this.timeouts.forEach(clearTimeout);
  }

  render() {
    return (
      <div
        ref={a => { this.n = a }}
        className='notification'
        style={this.state.style}>
        <div className='notification-inner-container'>
          <div>
            <div className='notification-title'>{this.props.title}</div>
            <div className='notification-message'>{this.props.message}</div>
          </div>
          <div>
            <a
              className="notification-closebtn"
              onClick={this.onClick}>
              &times;
            </a>
          </div>
        </div>
      </div>
    );
  }
}

Notification.propTypes = {
  enterTime: PropTypes.number,
  leaveTime: PropTypes.number,
  autoLeaveTime: PropTypes.number,
  title: PropTypes.string,
  message: PropTypes.string.isRequired
};

Notification.defaultProps = {
  enterTime: 1000,
  leaveTime: 1000,
  autoLeaveTime: 8000,
  title: 'Oops!'
};

export default Notification;
