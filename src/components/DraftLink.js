import React from 'react';

const CustomLink = ({ component, children, to, onClick }) => {
  return React.createElement (
    component,
    { to: to, onClick: onClick },
    children
  );
}

CustomLink.defaultProps = {
  component: 'a'
}

class DraftLink extends React.PureComponent {
    render() {
      const { children, to, onClick, component } = this.props;
      return (
        <CustomLink
          to={to}
          onClick={onClick}
          component={component}>
          {children}
        </CustomLink>
      );
    }
}

export default DraftLink;
