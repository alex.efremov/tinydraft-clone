import React from 'react';
import { withPropsOnChange } from 'recompose';

import DraftView from './DraftView';
import './DraftPreview.css';

const enhance = withPropsOnChange(
  ['value'],
  ({ value }) => ({
    value: value.length > 80 ? value.slice(0,80) + '...' : value
  })
);

const DraftPreview = (props) =>
  <div className='Draft-Preview-container'>
    <DraftView {...props} />
  </div>

export default enhance(DraftPreview);
