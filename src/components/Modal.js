import React from 'react';
import { compose, withHandlers, branch, renderNothing} from 'recompose';
import { Link } from 'react-router-dom';

import './Modal.css';

const enhance = compose(
  branch(({ isOpen }) => !isOpen, renderNothing),
  withHandlers({
    onYesClick: ({ onYesClick, draftIdToDelete }) => () => onYesClick(draftIdToDelete)
  })
);

const Modal = ({ onNoClick, onYesClick }) =>
  <div className="Modal">
    <div className="Modal-content">
      <p>Do you really want to delete this draft?</p>
      <div className='Modal-buttons'>
        <div className='Modal-button-container'>
          <Link to='/'
            onClick={onYesClick}
            className='Modal-button'>
            Yes
          </Link>
        </div>
        <div className='Modal-button-container'>
          <Link to='/' className='Modal-button' onClick={onNoClick}>
            No
          </Link>
        </div>
      </div>
    </div>
  </div>

export default enhance(Modal);
