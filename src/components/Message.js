import React from 'react';
import { defaultProps } from 'recompose';

const enhance = defaultProps({
  message: 'Page Not Found.'
});

const Message = ({ message }) =>
  <div className='message'>
    <h4>{message}</h4>
  </div>

export default enhance(Message);
