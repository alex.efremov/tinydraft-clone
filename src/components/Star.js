import React from 'react';
import { withProps } from 'recompose';

import './Star.css';

const enhance = withProps(({ faved }) => ({
  starType: faved ? 'fa-star' : 'fa-star-o'
}));

export default enhance(({ starType, className }) =>
  <i className={`Star fa ${starType} ${className}`} aria-hidden="true"></i>
)
