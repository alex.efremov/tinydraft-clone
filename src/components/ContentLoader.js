import React from 'react';
import ContentLoader, { Rect, Circle } from 'react-content-loader'

export default () =>
    <ContentLoader height={120} speed={1} primaryColor={'#404040'} secondaryColor={'#999'}>
      <Circle x={195} y={30} radius={15} />
      <Rect x={50} y={80} height={10} radius={5} width={300} />
      <Rect x={75} y={100} height={10} radius={5} width={250} />
    </ContentLoader>
