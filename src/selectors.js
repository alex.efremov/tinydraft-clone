import { createSelector } from 'reselect';
import _ from 'lodash';

const getShowFavorited = ({ visibilityFilter }) => visibilityFilter.show_favorited;
const getDrafts = ({ drafts }) => drafts;

export const getFilteredDrafts = createSelector(
  [ getShowFavorited, getDrafts ],
  (show_favorited, drafts) => {
    if (show_favorited) {
      return _.filter(drafts, { 'faved': true })
    }
    return drafts
  }
);
