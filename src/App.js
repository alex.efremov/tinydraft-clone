import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose, branch, lifecycle, renderComponent } from 'recompose';
import { connect } from 'react-redux';

import HeaderContainer from './containers/HeaderContainer';
import Routes from './Routes';
import './App.css';
import ContentLoader from './components/ContentLoader';
import { loadContent } from './actions';
import ErrorNotifications from './containers/ErrorNotifications';

const mapStateToProps = state => ({
  isContentLoaded: state.app.isContentLoaded,
});

const enhance = compose(
  withRouter,
  connect(mapStateToProps, { loadContent }),
  lifecycle({
    componentDidMount() {
      if (!this.props.isContentLoaded) {
        this.props.loadContent();
      }
    }
  }),
  branch(
    ({ isContentLoaded }) => !isContentLoaded, renderComponent(ContentLoader)
  )
);

const App = ({ isContentLoaded }) =>
  <div className="App">
    <ErrorNotifications/>
    <HeaderContainer />
    <Routes />
  </div>

export default enhance(App);
