import React from 'react';
import { connect } from 'react-redux';

import DraftInput from '../components/DraftInput';
import { addDraft } from '../actions';

export default connect(null, { addDraft })(({ addDraft }) =>
  <DraftInput onSaveClick={addDraft} />
);
