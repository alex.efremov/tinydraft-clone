import React from 'react';
import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';

import DraftInput from '../components/DraftInput';
import { updateDraft } from '../actions';

const mapStateToProps = ({ drafts }, { match: { params: { id } } }) => ({
  draft: drafts[id]
});

const enhance = compose(
  connect(mapStateToProps, { updateDraft }),
  withHandlers({
    saveDraft: ({ draft, updateDraft }) => value => {
      const newDraft = {
        ...draft,
        value
      };
      updateDraft(newDraft);
    }
  })
);

export default enhance(({ saveDraft, draft}) => {
    return (
      <DraftInput onSaveClick={saveDraft} value={draft.value} />
    );
  }
);
