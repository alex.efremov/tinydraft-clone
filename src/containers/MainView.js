import React from 'react';
import { connect } from 'react-redux';

import DraftList from '../components/DraftList';
import Modal from '../components/Modal';
import {
  toggleFavorDraft,
  markDraftToDelete,
  unmarkDraftToDelete,
  deleteDraft } from '../actions';
import { getFilteredDrafts } from '../selectors';

const mapStateToProps = (state) => ({
  drafts: getFilteredDrafts(state),
  isShown: state.app.isModalOpen,
  markedDraftIdToDelete: state.app.markedDraftIdToDelete
});

const MainVew = (({
  drafts,
  toggleFavorDraft,
  isShown,
  onYesClick,
  unmarkDraftToDelete,
  markDraftToDelete,
  deleteDraft,
  markedDraftIdToDelete
}) =>
  <div>
    <Modal
      isOpen={isShown}
      draftIdToDelete={markedDraftIdToDelete}
      onYesClick={deleteDraft}
      onNoClick={unmarkDraftToDelete} />
    <DraftList
      drafts={drafts}
      onDeleteClick={markDraftToDelete}
      onStartClick={toggleFavorDraft}/>
  </div>
);

export default  connect(mapStateToProps, {
  toggleFavorDraft,
  markDraftToDelete,
  unmarkDraftToDelete,
  deleteDraft })(MainVew);
