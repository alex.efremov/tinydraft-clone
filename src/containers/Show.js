import React from 'react';
import { connect } from 'react-redux';
import { compose, branch, renderComponent } from 'recompose';

import NotFound from '../components/404';
import DraftView from '../components/DraftView';
import Modal from '../components/Modal';
import {
  toggleFavorDraft,
  markDraftToDelete,
  unmarkDraftToDelete,
  deleteDraft } from '../actions';

const mapStateToProps = ({ drafts, app }, { match }) => ({
  draft: drafts[match.params.id],
  isShown: app.isModalOpen,
  markedDraftIdToDelete: app.markedDraftIdToDelete
});

const enhance = compose(
  connect(mapStateToProps, {
    toggleFavorDraft,
    markDraftToDelete,
    unmarkDraftToDelete,
    deleteDraft }),
  branch(
    ({ draft }) => !draft,
    renderComponent(NotFound)
  )
);

const Show = (props) =>
  <div>
    <Modal
      isOpen={props.isShown}
      draftIdToDelete={props.markedDraftIdToDelete}
      onYesClick={props.deleteDraft}
      onNoClick={props.unmarkDraftToDelete} />
    <div className='Draft-Show-container'>
      <DraftView
        {...props.draft}
        onDeleteClick={props.markDraftToDelete}
        onStarClick={props.toggleFavorDraft}
      />
    </div>
  </div>

export default enhance(Show);
