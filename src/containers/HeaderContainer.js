import React from 'react';
import { connect } from 'react-redux';

import { toggleShowFavorited, resetShowFavorited } from '../actions';
import Header from '../components/Header';

const mapStateToProps = ({ visibilityFilter: { show_favorited } }) => ({
  faved: show_favorited
});

const HeaderContainer = ({ faved, toggleShowFavorited, resetShowFavorited }) =>
  <Header
    faved={faved}
    onHeaderClick={resetShowFavorited}
    onStarClick={toggleShowFavorited} />

export default connect(
  mapStateToProps,
  { toggleShowFavorited, resetShowFavorited }
)(HeaderContainer);
