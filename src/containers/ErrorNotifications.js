import React from 'react';
import NotificationStack from '../components/NotificationStack';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  error: state.errors.drafts
})

class ErrorNotifications extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '',
      notifications: []
    };
  }

  componentWillReceiveProps({ error }) {
    if (error.title && error.message) {
      this.addNotification(error.title, error.message);
    }
  }

  addNotification = (title, message) => {
    return this.setState({
      notifications: [ ...this.state.notifications, {
        key: Date.now().toString(),
        title,
        message
      }]
    })
  }

  removeNotification = key => {
    this.setState({
      notifications: this.state.notifications.filter(n => n.key !== key)
    })
  }

  render() {
    return (
      <div>
        <NotificationStack
          notifications={this.state.notifications}
          handleRemoveNotification={this.removeNotification} />
      </div>
    );
  }
};

export default connect(mapStateToProps, null)(ErrorNotifications);
