import storage from '../storage';

export const addDraft = value => dispatch => {
  const draft = {
    id: Date.now().toString(),
    value,
    faved: false
  };
  return dispatch({
    type: 'ADD_DRAFT',
    payload: storage.setItem(draft.id, draft),
    meta: {
      draft
    }
  });
};

export const loadContent = () => dispatch => {
  return dispatch({
    type: 'LOAD_CONTENT',
    payload: storage.getAll()
  });
};

export const toggleFavorDraft = (id, value) => dispatch => {
  return dispatch({
    type: 'TOGGLE_FAVOR_DRAFT',
    payload: storage.updateByKey(id, 'faved', value),
    meta: {
      id
    }
  });
};

export const updateDraft = draft => dispatch => {
  return dispatch({
    type: 'UPDATE_DRAFT',
    payload: storage.setItem(draft.id, draft),
    meta: {
      draft
    }
  })
};

export const deleteDraft = id => dispatch => {
  return dispatch({
    type: 'DELETE_DRAFT',
    payload: storage.remove(id),
    meta: {
      id
    }
  });
};

export const toggleShowFavorited = () => ({
  type: 'TOGGLE_SHOW_FAVORITED'
});

export const resetShowFavorited = () => ({
  type: 'RESET_SHOW_FAVORITED'
});

export const markDraftToDelete = id => ({
  type: 'MARK_DRAFT_TO_DELETE',
  draftId: id
});

export const unmarkDraftToDelete = id => ({
  type: 'UNMARK_DRAFT_TO_DELETE'
});
