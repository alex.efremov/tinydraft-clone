const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'ADD_DRAFT_FULFILLED':
    case 'UPDATE_DRAFT_FULFILLED':
    case 'TOGGLE_FAVOR_DRAFT_FULFILLED':
      return {
        ...state,
        drafts: {}
      }
    case 'ADD_DRAFT_REJECTED':
    case 'UPDATE_DRAFT_REJECTED':
    case 'TOGGLE_FAVOR_DRAFT_REJECTED':
      return {
        ...state,
        drafts: {
          title: action.payload.name,
          message: action.payload.message
        }
      }
    default:
        return state;
  }
}
