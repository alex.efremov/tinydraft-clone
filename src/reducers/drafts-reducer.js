import _ from 'lodash';

const INITIAL_STATE = {};

const addNewDraft = (state, draft) => {
  return {
    [draft.id]: {
      ...draft
    },
    ...state
  };
};

const deleteDraft = (state, id) => _.omit(state, id);

const loadContent = (state, drafts) => {
  return {
    ...state,
    ...(_.mapKeys(drafts, 'id'))
  };
};

const toggleFavor = (state, id) => {
  return {
    ...state,
    [id]: {
      ...state[id],
      faved: !state[id].faved
    }
  };
};

const updateDraft = (state, draft) => {
  return {
    ...state,
    [draft.id]: {
      ...draft
    }
  };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'ADD_DRAFT_PENDING':
      return addNewDraft(state, action.meta.draft);
    case 'ADD_DRAFT_REJECTED':
      return deleteDraft(state, action.meta.draft.id);
    case 'DELETE_DRAFT_PENDING':
      return deleteDraft(state, action.meta.id);
    case 'LOAD_CONTENT_REJECTED':
      return state;
    case 'LOAD_CONTENT_FULFILLED':
      return loadContent(state, action.payload);
    case 'TOGGLE_FAVOR_DRAFT_PENDING':
    case 'TOGGLE_FAVOR_DRAFT_REJECTED':
      return toggleFavor(state, action.meta.id);
    case 'UPDATE_DRAFT_PENDING':
    case 'UPDATE_DRAFT_FULFILLED':
      return updateDraft(state, action.meta.draft);
    default:
      return state;
  }
}
