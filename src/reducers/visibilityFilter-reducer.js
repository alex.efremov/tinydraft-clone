const INITIAL_STATE = {
  show_favorited: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'TOGGLE_SHOW_FAVORITED':
      return {
        ...state,
        show_favorited: !state.show_favorited
      }
    case 'RESET_SHOW_FAVORITED':
      return {
        ...state,
        show_favorited: false
      }
    default:
      return state;
  }
}
