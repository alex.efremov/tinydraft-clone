import { combineReducers } from 'redux';

import app from './app-reducer';
import drafts from './drafts-reducer';
import errors from './errors-reducer';
import visibilityFilter from './visibilityFilter-reducer';

const rootReducer = combineReducers({
  app,
  drafts,
  errors,
  visibilityFilter
});

export default rootReducer;
