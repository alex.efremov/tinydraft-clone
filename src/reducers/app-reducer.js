const INITIAL_STATE = {
  isContentLoaded: false,
  isModalOpen: false,
  markedDraftIdToDelete: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'LOAD_CONTENT_FULFILLED':
      return {
        ...state,
        isContentLoaded: true
      }
    case 'MARK_DRAFT_TO_DELETE':
      return {
        ...state,
        isModalOpen: true,
        markedDraftIdToDelete: action.draftId
      }
    case 'UNMARK_DRAFT_TO_DELETE':
    case 'DELETE_DRAFT_PENDING':
      return {
        ...state,
        isModalOpen: false,
        markedDraftIdToDelete: null
      }
    default:
      return state;
  }
};
